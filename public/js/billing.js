Stripe.setPublishableKey('pk_live_s29RdzVXjk6aOscs8OvwA1Yo');
$('#billing-form').submit(function(e) {

	var $form = $(this);
	$form.find('.payment-errors').hide();
	$form.find('button').prop('disabled', true);

	Stripe.createToken($form, stripeResponseHandler);

	return false;

	function stripeResponseHandler(status, response) {

		var $form = $('#billing-form');

		if (response.error) {

			$form.find('.alert-box').show();
			$form.find('.payment-errors').text(response.error.message).show();
			$form.find('button').prop('disabled', false);
		} else {

			var token = response.id;

			$form.append($('<input type="hidden" name="stripeToken">').val(token));
			$form.get(0).submit();
		}
	};
});
