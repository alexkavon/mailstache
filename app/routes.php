<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function() {

	return View::make('home');
});

Route::get('features', function() {

	return View::make('features');
});

Route::get('pricing', function() {

	return View::make('pricing');
});

Route::get('docs', 'DocsController@start');
Route::get('docs/domain', 'DocsController@domain');
Route::get('docs/mailbox', 'DocsController@mailbox');
Route::get('docs/client', 'DocsController@client');
Route::get('docs/spam', 'DocsController@spam');
Route::get('docs/api/account', 'DocsController@apiAccount');

Route::get('help', function() {

	return View::make('help');
});

Route::get('irc', function() {

	return View::make('irc');
});

Route::get('about', function() {

	return View::make('about');
});

Route::get('tos', function() {

	return View::make('tos');
});

Route::get('privacy', function() {

	return View::make('privacy');
});

Route::get('security', function() {

	return View::make('security');
});

//Dashboard
Route::group(['before' => ['auth','banned','verify','card']], function() {

	Route::resource('aliases', 'AliasController');
	Route::resource('domains', 'DomainController');
	Route::resource('accounts', 'AccountController');
	Route::get('billing', 'BillingController@index');
	Route::post('billing/upgrade', 'BillingController@upgrade');
	Route::post('billing/cancel', 'BillingController@cancel');
	Route::post('billing/download/{invoice}', 'BillingController@download');
	Route::resource('support', 'SupportController');
	Route::resource('reply', 'ReplyController');
});

//Auth System
	Route::get('login', 'AuthController@login');
	Route::post('login', 'AuthController@auth');
	Route::get('logout', 'AuthController@logout');
	Route::get('password/reset', 'PasswordController@reset');
	Route::post('password/reset', 'PasswordController@passreset');
	Route::post('password/send', 'PasswordController@sendreset');

Route::group(['before' => ['auth','banned']], function() {

	Route::get('settings', 'UserController@index');
	Route::put('settings/password', 'UserController@password');
	Route::put('settings/phone', 'UserController@phone');
	Route::delete('settings', 'UserController@destroy');
	Route::put('settings/verify', 'UserController@verify');
	Route::post('message', 'UserController@message');
	Route::post('billing/new', 'BillingController@create');
});
