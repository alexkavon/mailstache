<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		Session::regenerateToken();
		throw new Illuminate\Session\TokenMismatchException;
	}
	Session::regenerateToken();
});


Route::filter('verify', function() {

	if (Auth::user()->level == 0) {

		return View::make('verify');
	}
});

Route::filter('banned', function() {

	if (Auth::user()->level == -1) {

		Auth::logout();
		return 'Your account has been banned!';
	}
});

Route::filter('card', function() {

	if (Auth::user() && ! Auth::user()->subscribed()) {

		return View::make('card');
	}
});

Route::filter('domain', function() {

	$user = Auth::user();
	$domains = Domain::where('user_id', $user->id)->get();

	if ($domains->count() == 2 && $user->stripe_plan == 'free') {

		return Redirect::route('domains.index')->with('message', 'You may only add 2 domains.');
	}
});

Route::filter('account', function() {

	$user = Auth::user();
	$accounts = Account::where('user_id', $user->id)->get();

	if ($accounts->count() == 4 && $user->stripe_plan == 'free') {

		return Redirect::back()->with('message', 'You man only add 4 mailboxes on this plan.');
	} elseif ($accounts->count() == 10 && $user->stripe_plan == 'toothbrush') {

		return Redirect::back()->with('message', 'You man only add 10 mailboxes on this plan.');
	} elseif ($accounts->count() == 50 && $user->stripe_plan == 'chevron') {

		return Redirect::back()->with('message', 'You man only add 50 mailboxes on this plan.');
	} elseif ($accounts->count() == 200 && $user->stripe_plan == 'fumanchu') {

		return Redirect::back()->with('message', 'You man only add 200 mailboxes on this plan.');
	}
});

Route::filter('updomain', function() {

	$domains = Domain::where('user_id',Auth::user()->id)->get();
	$count = $domains->count();
	$value = $count - 2;

	if ($count > 2) {

		return Redirect::back()->with('message', 'You currently need to remove ' . $value . ' domain(s) from your account before changing to that plan.');
	}
});

Route::filter('upaccount', function() {

	$limit = ['free' => 4,
		'toothbrush' => 10,
		'chevron' => 50,
		'fumanchu' => 200];
	$accounts = Account::where('user_id',Auth::user()->id)->get();
	$fut = Input::get('plan');

	$count = $accounts->count();
	$final = $limit[$fut];
	$value = $count - $final;

	if ($count > $final) {

		return Redirect::back()->with('message', 'You currently need to remove ' . $value . ' mailbox(es) from your account before changing to that plan.');
	}
});
