<?php

class AccountCreate {

	public function fire($job, $data) {

		$server = new \Fetch\Server('localhost', 143);
		$server->setAuthentication('cyrus-frontend', 'password');
		$server->createMailBox('user/' . $data['u'] . $data['d']);
		sleep(6);

		$job->delete();
	}

	public function folder($job, $data) {

		$server = new \Fetch\Server('localhost', 143);
		$server->setAuthentication('cyrus-frontend', 'password');
		$server->createMailBox('user/' . $data['u'] . '/Sent' . $data['d']);
		$server->createMailBox('user/' . $data['u'] . '/Drafts' . $data['d']);
		$server->createMailBox('user/' . $data['u'] . '/Spam' . $data['d']);
		$server->createMailBox('user/' . $data['u'] . '/Trash' . $data['d']);
		sleep(6);

		$job->delete();
	}

	public function perm($job, $data) {

		exec('python /srv/www/app/controllers/python/perm.py ' . $data['u'] . ' ' . $data['d']);

		$job->delete();
	}

	public function sieve($job, $data) {

		exec('python /srv/www/app/controllers/python/sieve.py ' . $data['u'] . ' ' . $data['d']);

		$job->delete();
	}

	public function upgrade($job, $data) {

		exec('python /srv/www/app/controllers/python/upgrade.py ' . $data['e'] . ' ' . $data['p']);

		$job->delete();
	}

	public function destroy($job, $data) {

		exec('python /srv/www/app/controllers/python/dmuser.py ' . $data['e']);

		$job->delete();
	}
}
