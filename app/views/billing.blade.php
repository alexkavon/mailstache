@extends('layouts.dashboard')

@section('content')

<?php
// Available plans
$plans = ['free' => 'Peach Fuzz ($0/mo)',
	'toothbrush' => 'Toothbrush ($5/mo)',
	'chevron' => 'Chevron ($25/mo)',
	'fumanchu' => 'Fu Manchu ($50/mo)'];
?>
<h1>Billing</h1>
@include('layouts.errors')
<section>
	<div class="large-6 columns">
		<h3>Plan</h3>
		<p><?php echo $plans[Auth::user()->stripe_plan]; ?></p>
		<h3>Credit Card</h3>
		<p>x<?php echo Auth::user()->last_four; ?></p>
	</div>
	<div class="large-6 columns">
		<h3>Change Plan</h3>
		<p>For more information on different plans, check out our <a href="/pricing" target="_blank">pricing</a>.</p>
		<?php echo Form::open(['action' => 'BillingController@upgrade']); ?>
			<select name="plan">
				<?php unset($plans[Auth::user()->stripe_plan]); ?>
				<?php foreach ($plans as $key => $value): ?>
				<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php endforeach; ?>
			</select>
			<input type="submit" value="Upgrade" class="button">
		<?php echo Form::close(); ?>
	</div>
	<div class="large-12 columns">
		<h3>Invoices</h3>
		<ul class="list">
		<?php foreach ($invoices as $inv): ?>
			<li><a href="#" data-reveal-id="<?php echo $inv->id; ?>-modal" data-reveal><?php echo $inv->dateString(); ?> <?php echo $inv->dollars(); ?></a></li>
			<div id="<?php echo $inv->id; ?>-modal" class="reveal-modal small" data-reveal>
      <h3>Download Invoice</h3>
      <?php echo Form::open(['action' => ['BillingController@download', $inv->id]]); ?>
        <input type="submit" value="Download" class="button expand">
      <?php echo Form::close(); ?>
      <a class="close-reveal-modal">&#215;</a>
    </div>
		<?php endforeach; ?>
		</ul>
	</div>
</section>

@stop
