@extends('layouts.main')

@section('content')

<div class="row">
	<div class="large-8 large-centered columns">
		<h1>Mailstache Security</h1>
		<h4 class="subheader">As a provider of hosted email services, Mailstache understands the importance of its customers privacy. To help ensure our customers have a sound mind about privacy we've implemented industry standard security measures within our architecture.</h4>
		<hr>
		<h2>Server Access</h2>
		<p>Only a select and trusted few of our engineering team actually have access to any of our servers. Furthermore, we do not currently run our own datacenter. Instead our machines are hosted by <a href="https://digitalocean.com/security" target="_blank">Digital Ocean</a>. We have good faith in their business and trust them to handling your data. Their network has been optimized to help defend against different attacks such as DDoS.</p>
		<h2>Secure Software</h2>
		<p>Another important aspect of server security is the software used. We only utilize software that places security as forefront concern. We utilize <a href="https://debian.org/security" target="_blank">Debian</a> as our OS of choice because security and bug fixes are available almost as soon as the vulnerability is reported.</p>
		<h2>Payment Security</h2>
		<p>Payment processing for Mailstache is handled by <a href="https://stripe.com/help/security" target="_blank">Stripe</a>. They are a well known name within the payment processing industry and handle thousands of websites ranging from small time blogs to large commerce websites. Stripe complies with PCI standards for storing and handling credit card information, which is needed for our subscription plans that we offer.</p>
		<h2>External & Internal Communication</h2>
		<p>For all our services we use encrypted connections. Anything internal is sent over a private networking address not available to the public. For external communication we enforce HTTPS (443), IMAPS (993), and submission (587 with STARTTLS enforced). We ensure browsers only talk to the Mailstache website and API over HTTPS by utilizing <a href="https://en.wikipedia.org/wiki/HTTP_Strict_Transport_Security" target="_blank">HSTS</a>. These connections are encrypted with TLS certificates and verified by a certificate authority. We routinely check the validity of our certificates and the configuration of our servers to ensure maximum protection.</p>
		<h2>Security Responsibility</h2>
		<p>Discovered vulnerabilities in our infrastructure are escalated to our highest priority. Vulnerabilities should be reported to <a href="mailto:security@mailstache.io">our security team</a> immediately (please utilize our PGP key below). We will return a human response about your issue within a 24-hour period, usually immediately. We take security issues very seriously and we ask that you do not release the information of the vulnerability publically until Mailstache has fully addressed it.</p>
		<p>Thank you in advance for helping keep Mailstache and its customers secure.</p>
		<h2>PGP Key</h2>
		<p>Not familiar with PGP Encryption? You can use our <a href="https://keybase.io/mailstache" target="_blank">Keybase profile</a> to sign and encrypt messages for us!
		<ul>
			<li><b>Key ID: </b>0x30AE8663</li>
			<li><b>Key Type: </b>RSA</li>
			<li><b>Key size: </b>2048</li>
			<li><b>Fingerprint: </b>62BC 8622 051A 8C25 799F 5D62 E664 99AE 30AE 8663</li>
			<li><b>User ID: </b>Mailstache Support <help@mailstache.io></li>
		</ul>
		<pre>-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (MingW32)

mQENBFM+DxoBCADGLPHUA1EJySbX/osA14W/uCYYdtPDIB4M0d4ITHnb3JhoseqY
c9Kg4AAiyatVBr9ZhpCvpiQISvN0WysnAF4Bv4RQhnAo6cnHtd8xguJHkoy7jf7z
6HmLRWe89Fb8XhaL/Uz4c4bSUgg9FXGTaFfAsdZHLJY1Rl0AHq5i9X30ixsO+4yu
f6c+vi3MRYAompRm3QHaPZStLdC9mfxqU12iSyv4quoFDeeexQY1ZuKUonYAmyZ5
fXCQn+k3Zlf9FALsdpGPC0F8bV9RPytpmgLo7HRWef3ZbPBKxtme7pu4bv/jFP7f
sDGHTOT1/dMHEwSu3ZAfuNlRdgNz8DtZCNXNABEBAAG0J01haWxzdGFjaGUgU3Vw
cG9ydCA8aGVscEBtYWlsc3RhY2hlLmlvPokBPwQTAQIAKQUCUz4PGgIbIwUJCWYB
gAcLCQgHAwIBBhUIAgkKCwQWAgMBAh4BAheAAAoJEOZkma4wroZj3REH/iVT8qAj
Mw3gVf/WkHcxiTlYVF3yECzDQC+Oqqo42Hsj8UtXJoH3GyWvOpYKpfQ5pzdlMsoT
EoH3dKLWbURub1ew+IzGxSg0ucDalU48/joiZHKfR1ZXKEbO9u48wGW+UCWDjhT/
Jpvwjzuqr+sLuATreCYAvnfDfC4V6MwxvJqWwrL9Ci+aZvOUkIr7lKdgmPrRL55M
9oZz3cfNtmVuj1neJQDfLtcTvt0rMbm6DgZtkMvAsfCq98cTEkXDwsGU/OINRuSY
DlTClBqI7ohD/9khlOk2OpK5AjwOdsKdlGZVYCzHzcAPFohMhtROl8vSRG+sNuLI
26dNsAjAsx/hySW5AQ0EUz4PGgEIANv71Xjv4OuqqzsmgKynsXfOXjMYOClsZggb
Q9MZNa6WFj4P26msns+9cfBtT2kgtWMfi3o/dSigFhqlLSzEVkNAxa1C7wsZOyvb
9QUEHmKe3Fk0PdUPVCRujMC2jyDYI4ZjL5sNr2kQzY0utFDQ19uPIQSD6yWy1W74
bDsjA2+8OKkDTBlt/zG9GOh+kXGBvqTzZ1f7Ac8eJNyVGcaudDy4+z0l5fT7zwtZ
NZqHVM5Yflc/G1Jd03H0RH4W2fVfGwwXs1Lf2c0QKPEyU28Dpl+JNSqhhipsyX/s
+Z1qQzHGMb+s0pN42I5t8Iy0OX5s/r10eCqEtgJQuoEzNndxOBEAEQEAAYkBJQQY
AQIADwUCUz4PGgIbDAUJCWYBgAAKCRDmZJmuMK6GYyuLCACsjKEafzilZvL2pswG
a1JBjKsjjOZgITvoUO+8YGHvevDmR1ZwVfG0oae42X+LtnAMef2znfNKym9A9uVg
Bnhbrne5QCNOzRz6xxNL5/l1VIO9BN1ZqaFw6tboIs3eflJC0rIbrAp0AxovNMnd
H3CAtXw++Jc6Pc4zLVNV0pJCZ3Ej4QhEahoUep/V1Xy+q3BnffgJRougbsFjAxO+
Do4wxQYaCqrKPOyfWWDN/FlMYk4Xfm9+YemFZS6MOtFH0LyqYP9b2f1fvwZXbPod
K+uU6nV74ps8QCHE/xLI16i70xD4MlTfbIAyWIVce13m5qIjyyUyROaKYfTbjgjh
0JkX
=48Zi
-----END PGP PUBLIC KEY BLOCK-----
		</pre>
	</div>
</div>

@stop
