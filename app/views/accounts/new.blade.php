@extends('layouts.dashboard')

@section('content')

<header>
	<h1>Add Account</h1>
</header>
<section id="users">
	@include('layouts.errors')
	<?php echo Form::open(['action' => 'AccountController@store']); ?>
		<label>Name</label>
		<input type="text" name="name" placeholder="John Doe">
		<div class="row">
			<div class="large-6 columns">
				<label>Email Address</label>
				<div class="row collapse">
					<div class="small-11 columns">
						<input type="text" name="username" placeholder="johndoe">
					</div>
					<div class="small-1 columns">
						<span class="postfix">@</span>
					</div>
				</div>
			</div>
			<div class="large-6 columns">
				<label>Domain</label>
				<?php echo Form::select('domain', $domains); ?>
			</div>
		</div>
		<label>Password</label>
		<input type="password" name="password" placeholder="********">
		<label>Confirm Password</label>
		<input type="password" name="password_confirmation" placeholder="********">
		<input type="submit" value="+ Add" class="button success">
	<?php echo Form::close(); ?>
</section>

@stop
