@extends('layouts.dashboard')

@section('content')

    <header>
        <h1>Accounts</h1>
    </header>
    <ul class="list">
        <?php foreach ($accounts as $account): ?>
        <li class="listing"><a href="/accounts/<?php echo $account->id; ?>"><?php echo $account->email; ?></a></li>
        <?php endforeach; ?>
    </ul>

@stop
