@extends('layouts.dashboard')

@section('content')

<h1><?php echo $account->email; ?></h1>
<div class="row">
	<div class="large-6 columns">
		<h3>Name</h3>
		<p><?php echo $account->name; ?></p>
	</div>
	<div class="large-6 columns">
		<h3>Update Password</h3>
		<?php echo Form::open(['action' => ['AccountController@update', $account->id], 'method' => 'put']); ?>
			<input type="password" name="password" placeholder="********">
			<input type="password" name="password_confirmation" placeholder="********">
			<input type="submit" value="Update Password" class="button expand success">
		<?php echo Form::close(); ?>
	</div>
</div>
<h2>Aliases</h2>
<ul class="list">
	<?php foreach($aliases as $alias): ?>
		<li><a href="#" data-reveal-id="myModal<?php echo $alias->id; ?>" data-reveal><?php echo $alias->alias; ?></a></li>
		<div id="myModal<?php echo $alias->id; ?>" class="reveal-modal small" data-reveal>
			<h3>Remove alias</h3>
			<p>To delete this alias, type <b>DELETE</b> into the box below.</p>
			<?php echo Form::open(['action' => ['AliasController@destroy', $alias->id], 'method' => 'delete']); ?>
				<input type="text" name="delete" placeholder="DELETE" required>
				<input type="submit" value="- Delete Alias" class="button alert expand">
			<?php echo Form::close(); ?>
			<a class="close-reveal-modal">&#215;</a>
		</div>
	<?php endforeach; ?>
</ul>
@include('layouts.errors')
<?php echo Form::open(['action' => 'AliasController@store']); ?>
	<div class="row">
		<div class="large-6 columns">
			<div class="row collapse">
				<div class="small-11 columns">
					<input type="text" name="alias" placeholder="jondoe">
				</div>
				<div class="small-1 columns">
					<span class="postfix">@</span>
				</div>
			</div>
		</div>
		<div class="large-6 columns">
			<?php echo Form::select('domain', $domains); ?>
		</div>
	</div>
	<select hidden name="email">
		<option value="<?php echo $account->email; ?>"></option>
	</select>
	<input type="submit" value="+ Add Alias" class="button success">
<?php echo Form::close(); ?>
<a href="#" data-reveal-id="deleteAccount" data-reveal class="button alert right">- Delete Mailbox</a>
<div id="deleteAccount" class="reveal-modal small" data-reveal>
	<h3>Delete Account</h3>
	<p>To delete this account, type <b>DELETE</b> into the box below.</p>
	<?php echo Form::open(['action' => ['AccountController@destroy', $account->id], 'method' => 'delete']); ?>
		<input type="text" name="delete" placeholder="DELETE" required>
		<input type="submit" value="- Delete Email Account" class="button alert expand">
	<?php echo Form::close(); ?>
	<a class="close-reveal-modal">&#215;</a>
</div>
@stop
