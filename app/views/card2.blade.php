@extends('layouts.dashboard')

@section('content')

<h1>Missing payment option!</h1>
<p>Your account requires adding a method of payment before we can allow you to use this service.</p>

<?php echo Form::open(['action' => 'BillingController@new', 'id' => 'billing-form']); ?>
	<div class="payment-errors" style="display:none;"></div>
	<select name="plan" required>
		<option value="free">Peach Fuzz ($0/mo)</option>
		<option value="toothbrush">Toothbrush ($5/mo)</option>
		<option value="chevron">Chevron ($15/mo)</option>
		<option value="fumanchu">Fu Manchu ($30/mo)</option>
	</select>
	<input type="tel" data-stripe="number" placeholder="Card Number">
	<input type="tel" data-stripe="cvc" placeholder="123">
	<?php echo Form::selectMonth(null, null, ['data-stripe' => 'exp-month']); ?>
	<?php echo Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year']); ?>
	<input type="submit" value="+ Add Card" class="button success">
<?php echo Form::close(); ?>

@stop
