@extends('layouts.main')

@section('content')

<section id="features" class="row">
	<div class="large-12 columns">
		<h1>About</h1>
	</div>
	<div class="large-12 columns">
		<h3>Where we hail from.</h3>
		<p>Mailstache is located in the beautiful state of Montana. It is owned by Infines, LLC.</p>
		<h3>How it's made.</h3>
		<p>Mailstache uses several open source projects. Some of which include:</p>
		<ul>
			<li>Debian 7</li>
			<li>Postfix</li>
			<li>Cyrus IMAP & SASL</li>
			<li>Laravel 4</li>
			<li>ZURB Foundation 5</li>
			<li>PostgreSQL</li>
			<li>SpamAsssassin</li>
			<li>OpenDKIM</li>
			<li>And more...</li>
		</ul>
	</div>
</section>

@stop
