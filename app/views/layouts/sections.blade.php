<div id="sidebar" class="large-2 columns hide-for-medium-down">
	<nav class="side-nav">
		<h5>Documentation</h5>
		<a href="/docs">Getting Started</a>
		<a href="/docs/domain">Domain Guide</a>
		<a href="/docs/mailbox">Mailboxes & Aliases</a>
		<a href="/docs/client">Client Setup</a>
		<a href="/docs/spam">Spam Prevention</a>
		<a href="/docs/#encrypt">Mail Encryption</a>
		<a href="/docs/api/account">Account API</a>
		<a href="/docs/#apisend">Transactional API</a>
	</nav>
</div>
