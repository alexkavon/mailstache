<section id="footer">
	<div class="row">
		<div class="large-12 columns">
			<span>&copy; Infines</span>
			<a href="/tos">Terms of Service</a> | <a href="/privacy">Privacy Policy</a> | <a href="/security">Security</a> | <a href="//twitter.com/mailstache" target="_blank">@mailstache</a>
		</div>
	</div>
</section>
