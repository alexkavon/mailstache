<div id="sidebar" class="large-2 columns hide-for-medium-down">
    <nav class="side-nav">
        <a href="../domains">Domains</a>
        <a href="../billing">Billing</a>
        <a href="../settings">Settings</a>
        <a href="../support">Support</a>
    </nav>
</div>
