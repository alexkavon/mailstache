<?php if (Session::has('message')): ?>
	<div data-alert class="alert-box alert">
		<span><?php echo Session::get('message'); ?></span>
	</div>
<?php endif; ?>
<?php if ($errors): ?>
	<?php foreach ($errors->all() as $error): ?>
	<div data-alert class="alert-box alert">
		<span><?php echo $error; ?></span>
		<a href="#" class="close">&times;</a>
	</div>
	<?php endforeach; ?>
<?php endif; ?>
