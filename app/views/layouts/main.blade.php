<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Mailstache</title>
		<meta name="description" content="Mailstache is a service designed for developers and small businesses who are tired of worrying about email. Our goal is to provide a simple outsourced email solution with high uptime.">
		<link href="/favicon.ico" rel="icon" type="image/x-icon">
		<link href='https://fonts.googleapis.com/css?family=Port+Lligat+Slab' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Crimson+Text:700italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/css/normalize.css">
		<link rel="stylesheet" type="text/css" href="/css/foundation.min.css">
		<link rel="stylesheet" type="text/css" href="/css/style.css">
		<link rel="apple-touch-icon" href="/avatar.png">
		<script src="/js/vendor/custom.modernizr.js"></script>
	</head>
	<body>
		<div class="off-canvas-wrap">
		<div class="inner-wrap">
		<nav class="top-bar hide-for-medium-down" data-topbar>
			<ul class="title-area">
				<li class="name"><h1><a href="/">Mailstache</a></h1></li>
				<li class="toggle-topbar menu-icon"><a><span></span></a></li>
			</ul>
			<section class="top-bar-section">
				<ul class="right">
					<li class="divider"></li>
					<li><a href="/features">Features</a></li>
					<li class="divider"></li>
					<li><a href="/pricing">Pricing</a></li>
					<li class="divider"></li>
					<li><a href="/docs">Docs</a></li>
					<li class="divider"></li>
					<li><a href="/help">Help</a></li>
					<li class="divider"></li>
					<li class="has-form"><a class="success button" href="/login"><?php if (Auth::check()) { echo 'Dashboard';} else { echo 'Log in';}; ?></a></li>
				</ul>
			</section>
		</nav>
		<nav class="tab-bar show-for-medium-down">
      <section class="left-small">
        <a class="left-off-canvas-toggle menu-icon"><span></span></a>
      </section>
      <section class="right tab-bar-section">
        <h1>Mailstache</h1>
      </section>
    </nav>
    <aside class="left-off-canvas-menu">
      <ul class="off-canvas-list">
        <li><label>Mailstache</label></li>
        <li><a href="/">Home</a></li>
        <li><a href="/features">Features</a></li>
        <li><a href="/pricing">Pricing</a></li>
        <li><a href="/help">Help</a></li>
        <?php if (Auth::check()): ?>
        <li class="divider"></li>
        <li><label>Dashboard</label></li>
        <li><a href="/domains">Domains</a></li>
        <li><a href="/billing">Billing</a></li>
        <li><a href="/settings">Settings</a></li>
        <li><a href="/support">Support</a></li>
        <li><a href="/logout">Logout</a></li>
        <?php else: ?>
        <li><a href="/login">Login</a></li>
        <?php endif; ?>
        <li class="divider"></li>
        <li><label>Docs</label></li>
        <li><a href="/docs">Getting Started</a></li>
        <li><a href="/docs/domain">Domain Guide</a></li>
        <li><a href="/docs/mailbox">Mailboxes & Aliases</a></li>
        <li><a href="/docs/client">Client Setup</a></li>
        <li><a href="/docs/spam">Spam Prevention</a></li>
        <li><a href="/docs/encrypt">Mail Encryption</a></li>
        <li><a href="/docs/api/account">Account API</a></li>
				<li><a href="/docs/api/send">Transactional API</a></li>
      </ul>
    </aside>
    <a class="exit-off-canvas" href="#"></a>
		@yield('content')
		@include('layouts.footer')
		<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="/js/foundation.min.js"></script>
		<script>
			$(document).foundation();
		</script>
		</div>
		</div>
	</body>
</html>
