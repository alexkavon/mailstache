@extends('layouts.dashboard')

@section('content')

<h1><?php echo $domain->domain; ?></h1>
<section class="verify">
	<div data-alert class="alert-box alert">
		<span>You need to verify ownership of your domain before you can continue!</span>
	</div>
	@include('layouts.errors')
	<p>To verify your domain, you'll need to add our MX records to your DNS. Keep in mind that depending on the <a href="http://lmgtfy.com/?q=TTL+DNS" target="_blank">TTL</a> of your DNS records, it may be up to 72 hours before you're able to verify.</p>
	<h3>MX Records</h3>
	<ul>
		<li>mx.mailstache.io. 1</li>
		<li>mx2.mailstache.io. 5</li>
		<li>mx3.mailstache.io. 5</li>
		<li>mx4.mailstache.io. 10</li>
		<li>mx5.mailstache.io. 10</li>
	</ul>
	<?php echo Form::open(['route' => ['domains.update', $domain->id], 'method' => 'put']); ?>
	<?php echo Form::submit('Verify Domain', ['class' => 'button']); ?>
	<?php echo Form::close(); ?>
	<a href="#" data-reveal-id="deleteDomain" data-reveal class="button alert right">- Delete Domain</a>
	<div id="deleteDomain" class="reveal-modal small" data-reveal>
		<h3>Removal Confirmation</h3>
		<p>To delete this domain and all associated data (mailboxes and aliases), type <b>DELETE</b> into the box below.</p>
		<?php echo Form::open(['action' => ['DomainController@destroy', $domain->id], 'method' => 'delete']); ?>
			<input type="text" name="delete" placeholder="DELETE" required>
			<input type="submit" value="- Delete Domain" class="button alert expand">
		<?php echo Form::close(); ?>
		<a class="close-reveal-modal">&#215;</a>
	</div>
</section>

@stop
