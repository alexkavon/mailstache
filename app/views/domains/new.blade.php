@extends('layouts.dashboard')

@section('content')

<h1>Add Domain</h1>
<section>
	@include('layouts.errors')
	<?php echo Form::open(['action' => 'DomainController@store']); ?>
		<label>Domain Name</label>
		<input type="text" name="domain" maxlength="253" placeholder="example.com">
		<input type="submit" value="+ Add" class="button success">
	<?php echo Form::close(); ?>
	<h3>Help</h3>
	<p>Be sure to include the full domain name (i.e. example.com). Do not include http://, https://, or www etc.
</section>

@stop
