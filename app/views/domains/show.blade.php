@extends('layouts.dashboard')

@section('content')

<h1><?php echo $domain->domain; ?></h1>
<?php if (count($accounts) > 0): ?>
	<ul class="list">
		<?php foreach($accounts as $account): ?>
		<li><a href="/accounts/<?php echo $account->id; ?>"><?php echo $account->email; ?></a></li>
		<?php endforeach; ?>
	</ul>
<?php else: ?>
	<p>A tumbleweed just rolled by...you best add a few accounts to this place.</p>
<?php endif; ?>
<a class="button success" href="../accounts">+ Add Mailbox</a>
<h3>DKIM Key</h3>
<input type="text" value="mailstache._domainkey">
<textarea>"v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvTeuQp9OqmmllE5i1rO2HiMjBzRYQTwIN1OqSFPzzCWKW1iSkZ2MEEHPHeoKOqir3LNvI/H7iOxvtMOYZ7oIGF0tROrzdokhJCMX4Ws1mr3UkD3Cxn1tOV6G76l0+pikbIXKYOPhhyzh2Zzin47dReytkwbq+BS0brCWBKs7mMQIDAQAB"</textarea>
<a href="#" data-reveal-id="deleteDomain" data-reveal class="button alert right">- Delete Domain</a>
<div id="deleteDomain" class="reveal-modal small" data-reveal>
	<h3>Removal Confirmation</h3>
	<p>To delete this domain and all associated data (mailboxes and aliases), type <b>DELETE</b> into the box below.</p>
	<?php echo Form::open(['action' => ['DomainController@destroy', $domain->id], 'method' => 'delete']); ?>
	<input type="text" name="delete" placeholder="DELETE" required>
	<input type="submit" value="- Delete Domain" class="button alert expand">
	<?php echo Form::close(); ?>
	<a class="close-reveal-modal">&#215;</a>
</div>

@stop
