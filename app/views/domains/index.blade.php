@extends('layouts.dashboard')

@section('content')

<h1>Domains</h1>
@include('layouts.errors')
<?php if (count($domains) > 0): ?>
	<ul class="list">
		<?php foreach ($domains as $domain): ?>
		<li><a href="/domains/<?php echo $domain->id; ?>"><?php echo $domain->domain; ?></a></li>
		<?php endforeach; ?>
	</ul>
	<?php echo $domains->links(); ?>
<?php else: ?>
	<p>Looks like you don't have any domains...</p>
<?php endif; ?>
<a class="button success" href="domains/create">+ Add Domain</a>

@stop
