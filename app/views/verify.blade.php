@extends('layouts.dashboard')

@section('content')

<header>
	<h1>Verify Yo 'Self!</h1>
</header>
<div data-alert class="alert-box warning">
	<span>Unverified Accounts are deleted after 3 months!</span>
</div>
<p>If you'd like to use our service you'll need to verify your phone number. You can do so by selecting a method below. Then you will receive either a text message or phone call with your verification code.</p>
<?php echo Form::open(['action' => 'UserController@message']); ?>
	<input type="submit" value="Text me!" class="button">
<?php echo Form::close(); ?>
@include('layouts.errors')
<?php echo Form::open(['action' => 'UserController@verify', 'method' => 'put']); ?>
	<input type="text" name="code" placeholder="12345678">
	<input type="submit" value="Verify" class="button success">
<?php echo Form::close(); ?>

@stop
