@extends('layouts.main')

@section('content')

<section id="features" class="row">
	<div class="large-12 columns">
		<div class="point row">
			<div class="large-4 columns">
			<h2 class="text-center">Built for Developers</h2>
			<p class="text-center"><i>Built for developers, by developers.</i> We know that phrase is over used, but Mailstache sprang from the mind of a developer in need. In recognizing that, we celebrate developers, because we were founded on sleepless nights too. Our goal is to provide affordable email services that developers will love.</p>
			<a href="/pricing" class="button large expand">Pricing</a>
			</div>
			<div class="large-8 columns">
			<img src="/images/code.jpg" class="th"/>
			</div>
		</div>
		<div class="point row">
			<div class="large-12 columns">
			<h2 class="text-center">It's a Cinch!</h2>
			<p class="text-center">Everything about Mailstache is built to be easy peasy lemon squeezey.<br />It's a snap to add, delete, or upgrade your mailboxes and aliases.</p>
			<img src="/images/dashboard.png" class="th" />
			</div>
		</div>
		<div class="point row">
			<div class="large-8 columns">
			<img src="/images/secure.jpg" class="th" />
			</div>
			<div class="large-4 columns">
			<h2 class="text-center">Safe & Sound</h2>
			<p class="text-center">We're not fans of selling or scanning the data on our servers. We take great lengths to ensure that others can't snoop around either. Furthermore we believe in educating our customers and the public on practices and methods of data security.</p>
			<a href="/security" class="button expand large success">Security Policy</a>
			</div>
		</div>
		<div class="point row">
			<div class="large-12 columns">
			<h2>To-do list</h2>
			<ul class="list">
				<p>This is our messy list of things we're adding! Watch our <a href="//twitter.com/mailstache" target="_blank">Twitter</a> for exciting news and updates.</p>
				<li><a>mail.mailstache.io Mega Weapon</a></li>
				<li><a>Status Monitor v1</a></li>
				<li><a>2FA Shields</a></li>
				<li><a>API & Documentation Unlock</a></li>
				<li><a>Spam-O-Tron</a></li>
				<li><a>Domain Blacklist Checker 5000</a></li>
				<li><a>Greylisting Power-Up</a></li>
				<li><a>Lost Analytics Relic</a></li>
			</ul>
			</div>
		</div>
	</div>
</section>

@stop
