@extends('layouts.dashboard')

@section('content')

<h1>Missing payment option!</h1>
<p>Your account requires adding a method of payment before we can allow you to use this service. Your card will not be charged.</p>
<div class="large-6 columns">
<?php echo Form::open(['action' => 'BillingController@create', 'id' => 'billing-form']); ?>
	<div data-alert class="alert-box alert" style="display:none;">
    <span class="payment-errors" style="display:none;"></span>
    <a href="#" class="close">&times;</a>
  </div>
	<div class="large-12 columns">
	<label>Plan</label>
	<select name="plan" required>
		<option value="free">Peach Fuzz ($0/mo)</option>
		<option value="toothbrush">Toothbrush ($5/mo)</option>
		<option value="chevron">Chevron ($25/mo)</option>
		<option value="fumanchu">Fu Manchu ($50/mo)</option>
	</select>
	</div>
	<div class="small-9 columns">
	<label>Credit Card</label>
	<input type="tel" data-stripe="number" placeholder="Card Number" required>
	</div>
	<div class="small-3 columns">
	<label>CVC Number</label>
	<input type="tel" data-stripe="cvc" placeholder="123" required>
	</div>
	<div class="small-9 columns">
	<?php echo Form::selectMonth(null, null, ['data-stripe' => 'exp-month']); ?>
	</div>
	<div class="small-3 columns">
	<?php echo Form::selectYear(null, date('Y'), date('Y') + 10, null, ['data-stripe' => 'exp-year']); ?>
	</div>
	<div class="small-8 columns">
		<input type="submit" value="+ Add Card" class="button success">
	</div>
	<div class="small-4 columns">
		<a href="https://stripe.com" target="_blank"><img src="/images/stripe.png" /></a>
	</div>
<?php echo Form::close(); ?>
</div>

@stop
