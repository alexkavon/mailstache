@extends('layouts.main')

@section('content')

<section id="hero">
	<div class="row">
		<div class="large-12 columns">
			<h1 class="text-center">Mailstache</h1>
			<h3 class="text-center">Simple email hosting for your domain.</h3>
		</div>
	</div>
</section>
<section id="about">
	<div class="row">
		<div class="large-4 columns">
			<h3>Built for Developers</h3>
			<img src="/images/code-small.jpg" class="th" />
			<p>Mailstache is a service designed for developers and small businesses that never want to mess with email again. Our goal is to provide simple outsourced hosted email services with high uptime.</p>
			<a href="/features" class="button">Learn More</a>
		</div>
		<div class="large-4 columns">
			<h3>Simple & Secure</h3>
			<img src="/images/secure-small.jpg" class="th" />
			<p>It's simple to create a new email address inbox for any domain you own. Furthermore all of our services are designed to be ad-free and secure from any tampering and data leaks.</p>
			<a href="/security" class="button success clearfix">Security Policy</a>
		</div>
		<div class="large-4 columns">
			<h3>Coming soon...</h3>
			<ul class="pricing-table">
        <li class="price">$0/mo</li>
        <li class="bullet-item">2 Domains</li>
        <li class="bullet-item">4 Mailboxes @ 1GB each</li>
        <li class="bullet-item">+10,000 messages</li>
        <li class="bullet-item">Mini stickers</li>
      </ul>
		</div>
	</div>
</section>
<section id="seen">
	<div class="row">
		<div class="large-12 columns">
			<h3 class="text-center">As Seen On:</h3>
			<h1 class="text-center">THE INTERNET</h1>
		</div>
	</div>
</section>

@stop
