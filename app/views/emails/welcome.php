<p>Hello!</p>

<p>You're <?php echo $email; ?> has been signed up for Mailstache! Mailstache is an email service designed for developers and small businesses to never have to worry about email again!</p>

<p>We're excited for you to never have to worry about email again! However, we need to verify that you do indeed exist and that you're not some nasty spammer. To do so, click the link below:</p>

<a href="http://mailstache.io/verify/<?php echo $vcode; ?>">Verify</a>

<p>Best,</p>
<p>The Mailstache Team</p>