<p>Hello!</p>

<p>You've added a new domain to your account: <?php echo $domain; ?></p>

<p>If you haven't authorized this action, please rememedy the situation through the dashboard. If you can't access your dashboard, please contact help@mailstache.io</p>

<p>Best,</p>
<p>The Mailstache Team</p>