@extends('layouts.main')

@section('content')

<section id="help" class="row">
	<div class="large-12 columns">
		<h1>Help</h1>
		<h3>What countries does Mailstache support?</h3>
		<p>Mailstache supports the following countries: Australia, Canada, Denmark, France, Germany, Mexico, New Zealand, Norway, Sweden, United Kingdom, and the United States.</p>
		<h3>Why do you need my phone number/credit card?</h3>
		<p>As we're an email company, asking you to provide an email address at sign up can lead to high levels of fraud. So instead we decided that a valid phone number and credit card would be required for the service. We would never sell your information or use it for advertising. We only will send a text message to your phone to help verify that you're not a bot. Your credit card never hits our servers. Mailstache utilizes <a href="https://stripe.com" target="_blank">Stripe</a> for payments.</p>
		<h3>When I switch to a cheaper plan, do I have to repay?</h3>
		<p>No way! Thanks to the brilliant people at Stripe, your account becomes prorated for the unused portion of your current plan. This also works if you switch to a more expensive plan.</p>
		<h3>How can I stop payments on my account?</h3>
		<p>To stop payments, you will need to either switch to our free plan or delete your Mailstache account. We do not offer refunds for cancelled services.</p>
		<h3>Where can I get my stickers?</h3>
		<p>To claim your stickers, please open a support ticket with the name and address.</p>
		<h3>How many email aliases can I have?</h3>
		<p>Each email address can have up to five aliases. This limit can not be increased.</p>
		<h3>What are the incoming and outgoing message size limits?</h3>
		<p>Mailstache offers an incoming and outgoing message size limit of 25mB per message. This limit is to help reduce load on our servers. If you need to send large attachments, we suggest utilizing an online file storage service.</p>
		<h3>Some messages land in the Spam folder. Why is this?</h3>
		<p>Mailstache wants partake in the battle against spam. By doing so, any message that isn't secured and verified by SPF and DKIM will land in your spam box. These are just the primary idicators against spam, messages are also susceptible to our spam filter.</p>
		<h3>Gimme dat SPF record!</h3>
		<p>First of all, it's <i><b>give me that</b> SPF record</i> and secondly try to use <i>please</i> next time. The SPF record is: <b>"v=spf1 mx a a:spf.mailstache.io -all"</b></p>
		<h3>I need more help!</h3>
		<p>You can contact us in many ways:</p>
		<ul>
			<li><a href="mailto:help@mailstache.io">Email us</a></li>
			<li><a href="/irc">#mailstache on IRC</a></li>
			<li><a href="//twitter.com/mailstache" target="_blank">Twitter</a></li>
			<li><a href="/support">Open a support ticket</a></li>
		</ul>
	</div>
</section>

@stop
