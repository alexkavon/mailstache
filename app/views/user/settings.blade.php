@extends('layouts.dashboard')

@section('content')

<h1>Settings</h1>
<section id="settings">
	<div class="row">
		@include('layouts.errors')
		<div id="info" class="large-6 columns">
			<h3>Username</h3>
			<p><?php echo Auth::user()->username; ?></p>
			<h3>Phone</h3>
			<p>+<?php echo Auth::user()->phone; ?></p>
			<h3>API Key</h3>
			<p>Coming Soon.</p>
			<h3>API Secret</h3>
			<p>Coming Soon.</p>
			<a href="#" class="button alert" data-reveal-id="deleteModal" data-reveal>- Delete Account</a>
			<div id="deleteModal" class="reveal-modal small" data-reveal>
				<h3>Delete account</h3>
				<p>By doing so, we will remove all your account information and email from our database. To confirm type <b>DELETE</b>.</p>
				<?php echo Form::open(['action' => 'UserController@destroy', 'method' => 'delete']); ?>
					<input type="text" name="delete" placeholder="DELETE" required>
					<input type="submit" value="- Delete Account" class="button alert expand">
				<?php echo Form::close(); ?>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
		<div id="settings" class="large-6 columns">
			<h3>Update Phone</h3>
			<?php echo Form::open(['action' => 'UserController@phone', 'method' => 'put']); ?>
				<div class="row collapse">
					<div class="small-1 columns">
						<span class="prefix">+</span>
					</div>
					<div class="small-11 columns">
						<input type="tel" name="phone" maxlength="15" placeholder="<?php echo $user->phone; ?>">
					</div>
				</div>
				<input type="submit" value="Update Phone" class="button success expand">
			<?php echo Form::close(); ?>
			<h3>Update Password</h3>
			<?php echo Form::open(['action' => 'UserController@password', 'method' => 'put']); ?>
				<label>New Password</label>
				<input type="password" name="password" placeholder="********">
				<label>Confirm New Password</label>
				<input type="password" name="password_confirmation" placeholder="********">
				<input type="submit" value="Update Password" class="button success expand">
			<?php echo Form::close(); ?>
		</div>
	</div>
</section>

@stop
