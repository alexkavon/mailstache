@extends('layouts.main')

@section('content')

<section id="auth" class="row">
	<div class="large-4 large-centered columns">
		<h1>Password Reset</h1>
		@include('layouts.errors')
		<?php echo Form::open(['action' => 'PasswordController@passreset']); ?>
			<label>Username</label>
			<input type="text" max="15" name="username" placeholder="johndoe">
			<input type="submit" value="Send Verification" class="button success expand">
		<?php echo Form::close(); ?>
	</div>
</section>

@stop
