@extends('layouts.main')

@section('content')

<section id="auth" class="row">
	<div class="large-4 large-centered columns">
		<h1>Confirm Reset</h1>
		<?php echo Form::open(['action' => 'PasswordController@sendreset']); ?>
			<input type="text" name="code" maxlength="8" placeholder="12345678">
			<input type="hidden" name="username" value="<?php echo $username; ?>">
			<input type="submit" value="Verify" class="button success expand">
		<?php echo Form::close(); ?>
	</div>
</section>

@stop
