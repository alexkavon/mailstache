@extends('layouts.main')

@section('content')

<section id="irc" class="row">
	<div class="large-12 columns">
		<h1>Community IRC</h1>
	</div>
	<div class="large-6 columns">
		<h3>Code of Conduct</h3>
		<p>The following is required to be treated with respect within our IRC Channel:</p>
		<ul>
			<li><b>Don't ask to ask.</b> Just state your question and we may be of assistance. This includes questions like: "Is anyone here?" or "Can anyone help me?"</li>
			<li><b>Be nice.</b> Don't enter our chatroom and demand that someone help you. This is a public community chat room. Where people like to hang out and discuss things. It is <b>NOT</b> tech support.</li>
			<li><b>Help yourself before trying us.</b> 9/10 times your question will be pulled from a search engine or our documentation section. Don't be offended when you ask a question only to get a reply with a <a href="http://lmgtfy.com/?q=lmgtfy" target="_blank">lmgtfy</a>.</li>
			<li><b>Get help, then help others.</b> If you find us helpful or cool, feel free to stick around and contribute to the awesomeness of the channel.</li>
		</ul>
	</div>
	<div class="large-6 columns">
		<h3>Banning policy</h3>
		<p>If you are rowdy and offensive then you will receive the following course of actions taken against you:</p>
		<ol>
			<li>A warning</li>
			<li>Kicked out (though you may return with a better attitude)</li>
			<li>Permanent ban</li>
		</ol>
		<a href="https://kiwiirc.com/client/irc.freenode.net/mailstache" target="_blank" class="button">Ok</a>
	</div>
</section>

@stop
