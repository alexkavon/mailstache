@extends('layouts.main')

@section('content')

<section id="auth" class="row">
	<div class="large-4 large-centered columns">
		<h1>Sign Up</h1>
		@include('layouts.errors')
		<?php echo Form::open(['action' => 'AuthController@register']); ?>
			<label>Username</label>
			<input type="text" name="username" maxlength="15" placeholder="johndoe">
			<label>Phone Number</label>
			<div class="row collapse">
				<div class="small-1 columns">
					<span class="prefix">+</span>
				</div>
				<div class="small-11 columns">
					<input type="tel" name="phone" maxlength="15" placeholder="12345678910">
				</div>
			</div>
			<p><i>Your phone number must be in <a href="http://en.wikipedia.org/wiki/E.164" target="_blank">E.164</a> format.</i></p>
			<p><i>Mailstache supports the following countries: Australia, Canada, Denmark, France, Germany, Mexico, New Zealand, Norway, Sweden, United Kingdom, and the United States.</i></p>
			<label>Password</label>
			<input type="password" name="password" placeholder="********">
			<label>Confirm Password</label>
			<input type="password" name="password_confirmation" placeholder="********">
			<input type="submit" value="Sign Up" class="button success expand">
			<p><i>By clicking submit, you agree to our <a href="../tos" target="_blank">Terms of Service</a>.</i></p>
		<?php echo Form::close(); ?>
	</div>
	<div class="suggest large-4 large-centered columns">
		<p class="text-center">Already have an account? <a class="button tiny" href="../login">Log in</a></p>
		<p class="text-center">Forgot your password? <a class="button tiny alert" href="/password/reset">Reset</a></p>
	</div>
</section>
@stop
