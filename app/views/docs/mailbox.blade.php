@extends('layouts.docs')

@section('content')

<div class="large-12 columns">
	<h1>Mailboxes & Aliases</h1>
	<p>After <a href="/docs/domain">adding and verifying a domain</a>, you can begin adding mailboxes. Each mailbox receives five main folders which are an inbox, sent, drafts, spam, trash folder. These main folders can not be modified or deleted but mailboxes are welcome to have more folders.</p>
	<h2>Adding a mailbox</h2>
	<p>To add a mailbox, log into your Mailstache dashboard and click on the domain you wish to add a mailbox under. Then click the <b>+ Add Account</b> button. This should bring you to the new mailbox page.</p>
	<img src="/images/docs/domain4.png" class="th"/>
	<p></p>
	<p>Simply fill out this form and Mailstache will begin setting up the new mailbox!</p>
	<img src="/images/docs/email1.png" class="th"/>
	<p></p>
	<p>Afterwards you will be returned to the domain view. By clicking on the mailbox, you will be taken to the mailbox display page where you may update/delete the mailbox as well as add/remove aliases.</p>
	<img src="/images/docs/email2.png" class="th"/>
	<p></p>
	<p>Once you've completed updating your mailbox, you will want to setup a client in order to send and receive email messages.</p>
	<a href="/docs/client" class="button success">Configure a Client</a>
</div>

@stop
