@extends('layouts.docs')

@section('content')

<div class="large-12 columns">
	<h1>Domain Guide</h1>
	<p>Adding a domain is incredibly easy. To add a new domain to your account, you will need access to the DNS records for said domain.</p>
	<h2>Step One - Add a Domain</h2>
	<img src="/images/docs/domain1.png" class="th"/>
	<p></p>
	<p>From the <b>Domains</b> section of your dashboard click the <b>+ Add Domain</b> button. This will take you to the domain add form. Simply add your domain name into this form and be sure to include the full domain name (i.e. example.com). Do not include http://, https://, or www etc. Then click the <b>+ Add</b> to move on to the next step.</p>
	<img src="/images/docs/domain2.png" class="th"/>
	<h2>Step Two - Domain Verification</h2>
	<p>Verification of a domain can either be quick or instantaneous, depending on how your DNS is configured. You'll need to check with your hosting provider to find out how long it takes for your DNS records to be updated. You will also need access to your domain's DNS records to asign a specific kind of record called an MX record.</p>
	<h3>What is an MX record?</h3>
	<p>Mail exchanger records, often abbreviated as MX records, are a type of DNS record. The sole purpose of an MX record is to tell an incoming email message what server will handle that message for that domain. For example if you are sending a message to bob@example.com, the sending server will first query example.com for its MX record. If example.com were pointed at Mailstache, then it would return a result of mx.mailstache.io (1), mx2.mailstache.io (5), mx3.mailstache.io (5), etc. The MX record then tells the sending server that any email for example.com will be handled by mx.mailstache.io first because it contains a priority of 1. This is because MX records also contain priority information in the case that multiple mail servers are available. If mx.mailstache.io were busy, then the sending server would contact mx2.mailstache.io and so forth.</p>
	<h3>Verifying your domain</h3>
	<p>Clicking on your domain in the Mailstache dashboard will notify you that you need to verify your domain before continuing to add mailboxes.</p>
	<img src="/images/docs/domain3.png" class="th"/>
	<p></p>
	<p>To start, you will need to login to wherever your DNS is managed. This can be your hosting company or your domain registrar. Once you have obtained access, you will need to add the following records.</p>
	<p><b>NOTE: As each DNS management dashboard is usually we ask that you consult your hosts documentation on adding records.</b></p>
	<table>
		<th>MX Record</th><th>Priority</th>
		<tr><td>mx.mailstache.io</td><td>1</td></tr>
		<tr><td>mx2.mailstache.io</td><td>5</td></tr>
		<tr><td>mx3.mailstache.io</td><td>5</td></tr>
		<tr><td>mx4.mailstache.io</td><td>10</td></tr>
		<tr><td>mx5.mailstache.io</td><td>10</td></tr>
	</table>
	<p>Some DNS dashboards will as for a TTL (Time to Live) value. This is simply the amount of time that the record will be considered valid before a refresh. You may set this as high or low as your DNS dashboard allows.</p>
	<p>After the records are added, you will need to wait for your DNS dashboard to reset it's cached values. Depending on where your DNS is managed, this can take any where from 1 second to 72 hours (however this usually takes no more than 24 hours). You can check to see if your records have been updated by running a <a href="http://en.wikipedia.org/wiki/Dig_(Command)" target="_blank">dig</a> command from your favorite terminal. Once those records have updated, simply click the <b>Verify Domain</b> button.</p>
	<img src="/images/docs/domain4.png" class="th"/>
	<p></p>
	<p>Doing so will allow you start adding mailboxes and begin receiving email!</p>
	<a href="/docs/mailbox" class="button success">Add a Mailbox</a>
</div>

@stop
