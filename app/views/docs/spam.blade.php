@extends('layouts.docs')

@section('content')

<div class="large-12 columns">
	<h1>Spam Protection & Prevention</h1>
	<p>Spam protection and prevention measures are an absolute must of any email service. Mailstache takes measures to help ensure your inbox stays spam-free. However it's also import to configure your domain's DNS records so your messages aren't recognized as spam as well. It's easy to do so by adding each of the following records: DKIM, SPF, and DMARC.</p>
	<h2>Reccomended DNS Providers</h2>
	<p>Mailstache utilizes stronger DKIM keys that may not be supported by some domain registrars or hosting companies DNS implementation. Due to some hosts poor implementation we suggest changing your DNS host to a more suitable provider. Here are a few we think are fantastic for the job:</p>
	<ul>
		<li><a href="https://gandi.net" target="_blank">Gandi DNS</a></li>
		<li><a href="http://dns.he.net" target="_blank">Hurricane Electric DNS</a></li>
		<li><a href="http://cloudflare.com" target="_blank">Cloudflare DNS</a></li>
	</ul>
	<h2>DKIM<h2>
	<p>DKIM, or DomainKeys Identified Mail, is a method of signing email messages and associating them with a domain. DKIM allows a receiving server to verify that the email message was intentionally sent by the domain associated with it.</p>
	<p>To add a DKIM record to your DNS, start by logging into your DNS provider and your Mailstache dashboard. In your DNS host add a new TXT record. The TXT record should require two values, the first is called a <i>selector</i> which provides a human readable name to a key. The second value is the <i>public key</i> of the DKIM set. Both values can be found within the domain settings of the domain you'd like to begin protecting.</p>
	<img src="/images/dkim1.png" class="th" />
	<p></p>
	<p>Simply paste each value into it's corresponding record value.</p>
	<img src="/images/dkim2.png" class="th" />
	<p></p>
	<h2>SPF</h2>
	<p>Coming soon...</p>
	<h2>DMARC</h2>
	<p>Coming soon...</p>
	<a href="/docs/encrypt" class="button success">Message Encryption & Signing</a>
</div>

@stop
