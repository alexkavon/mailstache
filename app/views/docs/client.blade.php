@extends('layouts.docs')

@section('content')

<div class="large-12 columns">
	<h1>Client Guide</h1>
	<p>Below are several guides for different email clients we think work well with Mailstache. There are many many email clients that exist, however we can not possibly maintain guides for all of them. If you'd like us to add a guide for an email client please contact us.</p>
	<hr>
	<div class="magellan-container" data-magellan-expedition="fixed">
		<dl class="sub-nav">
			<dd data-magellan-arrival="thunderbird"><a href="#thunderbird">Mozilla Thunderbird</a></dd>
		</dl>
	</div>
	<hr>
  <a name="thunderbird"></a>
	<h2 data-magellan-destination="thunderbird">Mozilla Thunderbird</h2>
	<p><b>Note: </b>You will need the email address and password of the mailbox you're trying to set up. These are not the same username and password that you use to log into the Mailstache dashboard. If you forgot the email addresses password, you may reset this on the mailbox page in the dashboard.</p>
	<img src="/images/docs/client1.png" class="th" />
	<p></p>
	<p>First access the <b>File</b> menu and select the <b>New</b> submenu and select the <b>Existing Mail Account...</b> option. This will open the <b>Mail Account Setup</b> dialog.</p>
	<img src="/images/docs/client2.png" class="th" />
	<p></p>
	<p>Enter the details (<i>Email address</i> and <i>Password</i> fields are required, however the <i>Your name</i> field is optional) of the mailbox you'd like to access and select <b>Continue</b>. This should begin the automatic configuration process. Simply select the <b>Manual config</b> button that appears to continue proper setup, or let the process fail.</p>
	<img src="/images/docs/client3.png" class="th" />
	<p></p>
	<p>Once you've entered manual configuration, Thunderbird will present you with fields to fill in the details manually. Enter the following:</p>
	<h3>Incoming settings</h3>
	<p>These settings will allow you to access your Mailstache hosted mailbox.</p>
	<ul>
		<li><b>Server type: </b>IMAP</li>
		<li><b>Server hostname: </b>imap.mailstache.io</li>
		<li><b>Port: </b>993</li>
		<li><b>SSL: </b>SSL/TLS</li>
		<li><b>Authentication: </b>Normal password</li>
	</ul>
	<h3>Outgoing settings</h3>
	<p>These settings will allow you to send email from your Mailstache hosted mailbox.</p>
	<ul>
		<li><b>Server type: </b>SMTP</li>
		<li><b>Server hostname: </b>smtp.mailstache.io</li>
		<li><b>Port: </b>587</li>
		<li><b>SSL: </b>STARTTLS</li>
		<li><b>Authentication: </b>Normal password</li>
	</ul>
	<h3>Username setting</h3>
	<p>This setting often confuses people it's ambiguity stems for the long and boring history of wide email server configuration options. For Mailstache this value should always be the mailbox email address you're trying to access.</p>
	<img src="/images/docs/client4.png" class="th" />
	<p></p>
	<p>After entering the details, select the <b>Re-test</b> button. If the test fails look for any mistakes and try again, otherwise click <b>Done</b>. Your Mailstache mailbox should now work with Thunderbird.</b>
	<img src="/images/docs/client5.png" class="th" />
	<p></p>
	<p>Since Thunderbird doesn't load folders automatically, you'll need to subscribe to the automatically created folders of your mailbox. From the <b>File</b> menu select the <b>Subscribe...</b> option. Then click the <b>Subscribe</b> for each folder to gain access.</p>
	<img src="/images/docs/client6.png" class="th" />
	<p></p>
	<p>Finally click the <b>OK</b> button to complete the setup of your new mailbox in Thunderbird. Now that you've successfully have setup a mailbox, you can move on to securing your email and the rest of the internet from spam.</p>
	<a href="/docs/spam" class="success button">Spam Protection</a>
</div>

@stop
