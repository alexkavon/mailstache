@extends('layouts.docs')

@section('content')

<div class="large-12 columns">
	<h1>Getting Started</h1>
	<p>Mailstache has been designed to be easy to use and implement. It takes only a few minutes to get an email address up and working. Whether you're a small time developer or startup looking for an easy low cost email solution or you're a big time blog sending out a monthly newsletter, we've provided all the documentation to do just that!</p>
	<hr>
	<h2>Easy as 1-2-3</h2>
	<div class="large-4 columns">
		<h3>1. Add a Domain</h3>
		<p>The first step after account activation is adding and verifying a domain. This guide will help you get up and running quickly.</p>
		<a href="/docs/domain" class="button">Domain Guide</a>
	</div>
	<div class="large-4 columns">
		<h3>2. Mailboxes & Aliases</h3>
		<p>Adding mailboxes is so easy, that a...well it's really simple. Here's nice little step by step to get you going.</p>
		<a href="/docs/mailbox" class="button">Mailbox Guide</a>
	</div>
	<div class="large-4 columns">
		<h3>3. Configure a Client</h3>
		<p>Finally you'll need to check your mail. Here's a few guides to checking mail with your favorite client.</p>
		<a href="/docs/client" class="button">Client Guide</a>
	</div>
	<hr>
	<h2>Protect Yourself Against Spam</h2>
	<p>Like most email providers, Mailstache provides spam detection and prevention services baked into mailboxes. However it's important (and rather easy) to configure your domain so other email systems don't recognize the email you're sending out as spam. This can include additions such as DKIM, SPF, DMARC, etc. We've written up a pretty nifty and easy setup of how to keep your domain name in the good graces of others (and don't worry, we'll explain everything along the way).</p>
	<a href="/docs/spam" class="button success">Fight Spam Now</a>
	<hr>
	<h2>Upgrade Your Privacy</h2>
	<div class="large-6 columns">
		<h3>GPG Encryption</h3>
		<p>Coming soon...</p>
	</div>
	<div class="large-6 columns">
		<h3>S/MIME Encryption</h3>
		<p>Coming soon...</p>
	</div>
	<hr>
	<h2>Build Something Cool</h2>
	<div class="large-6 columns">
		<h3>Account API</h3>
		<p>We've built a cool little API to help manage your account. It's the easiest (non-dashboard) way to add domains, mailboxes, and aliases.</p>
		<a href="/docs/api/account" class="button alert">Account API</a>
	</div>
	<div class="large-6 columns">
		<h3>Transactional API</h3>
		<p>Coming soon...</p>
	</div>
</div>

@stop
