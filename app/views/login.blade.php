@extends('layouts.main')

@section('content')

<section id="auth" class="row">
	<div class="large-4 large-centered columns">
		<h1>Log In</h1>
		@include('layouts.errors')
		<?php echo Form::open(['action' => 'AuthController@auth']); ?>
			<label>Username</label>
			<input type="text" name="username" maxlength="15" placeholder="johndoe">
			<label>Password</label>
			<input type="password" name="password" placeholder="********">
			<input type="submit" value="Log In" class="button success expand">
		<?php echo Form::close(); ?>
	</div>
	<div class="suggestion large-4 large-centered columns">
		<p class="text-center">Forgot your password? <a class="button tiny alert" href="/password/reset">Reset</a></p>
	</div>
</section>

@stop
