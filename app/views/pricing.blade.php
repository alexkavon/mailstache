@extends('layouts.main')

@section('content')

<section id="prices" class="row">
	<div class="large-12 columns">
		<h1>Pricing</h1>
	</div>
	<div class="large-3 columns">
		<ul class="pricing-table">
			<li class="title">Peach Fuzz</li>
			<li class="price">$0/mo</li>
			<li class="bullet-item">Free</li>
			<li class="bullet-item">2 Domains</li>
			<li class="bullet-item">4 Mailboxes @ 1GB each</li>
			<li class="bullet-item">+10,000 messages</li>
			<li class="bullet-item">Mini stickers</li>
		</ul>
	</div>
	<div class="large-3 columns">
		<ul class="pricing-table">
			<li class="title">Toothbrush</li>
			<li class="price">$5/mo<li>
			<li class="bullet-item">$60/yr</li>
			<li class="bullet-item">Unlimited domains</li>
			<li class="bullet-item">10 Mailboxes @ 5GB each</li>
			<li class="bullet-item">+20,000 messages</li>
			<li class="bullet-item">Little stickers</li>
	</div>
	<div class="large-3 columns">
		<ul class="pricing-table">
			<li class="title">Chevron</li>
			<li class="price">$25/mo</li>
			<li class="bullet-item">$300/yr</li>
			<li class="bullet-item">Unlimited domains</li>
			<li class="bullet-item">50 Mailboxes @ 10GB each</li>
			<li class="bullet-item">+50,000 messages</li>
			<li class="bullet-item">Big + Little Stickers</li>
		</ul>
	</div>
	<div class="large-3 columns">
		<ul class="pricing-table">
			<li class="title">Fu Manchu</li>
			<li class="price">$50/mo</li>
			<li class="bullet-item">$600/yr</li>
			<li class="bullet-item">Unlimited domains</li>
			<li class="bullet-item">200 Mailboxes @ 10GB each</li>
			<li class="bullet-item">+100,000 messages</li>
			<li class="bullet-item">Big + Little Stickers + T-shirt</li>
		</ul>
	</div>
	<div class="large-3 columns">
		<div class="panel callout">
			<h2>Do you need a whole bunch more? Try the Walrus.</h2>
		</div>
	</div>
	<div class="large-3 columns">
		<div class="panel">
			<h2>Unlimited domains and 10GB mailboxes.</h2>
		</div>
	</div>
	<div class="large-3 columns">
		<div class="panel">
			<h2>A whopping +5,000,000 messages.</h2>
		</div>
	</div>
	<div class="large-3 columns">
		<div class="panel">
			<h2>For a low low price.</h2>
		</div>
	</div>
</section>

@stop
