@extends('layouts.dashboard')

@section('content')

<h1>Support</h1>
<p>Before opening a ticket please consider trying these options:</p>
<ul>
	<li>Your answer may be in the FAQs in the <a href="../help" target="_blank">Help section</a></li>
	<li>For API information and how to guides, try the <a href="../docs" target="_blank">Documentation</a></li>
	<li>For community discussion we have <a href="../irc">#mailstache</a></li>
</ul>

<h3>Open a Ticket</h3>
@include('layouts.errors')
<?php echo Form::open(['action' => 'SupportController@store']); ?>
	<input type="text" name="topic" maxlength="50" placeholder="What's your beef?">
	<textarea  name="reply" maxlength="3000" placeholder="Tell us about it."></textarea>
	<input type="submit" value="{ Open Ticket" class="button success">
<?php Form::close(); ?>
<h3>Tickets</h3>
<p><i>Old tickets are removed every 6 months.</i></p>
<?php if (count($tickets) > 0): ?>
	<ul class="list">
		<?php foreach ($tickets as $ticket): ?>
		<li class="listing"><a href="/support/<?php echo $ticket->id; ?>"><?php echo $ticket->topic; ?><?php if ($ticket->status == 0) { echo ' - Closed'; } ?></a></li>
		<?php endforeach; ?>
	</ul>
<?php else: ?>
	<p>Looks like you don't have any tickets! Hooray!</p>
<?php endif; ?>
</section>


@stop
