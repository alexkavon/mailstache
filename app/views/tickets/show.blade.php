@extends('layouts.dashboard')

@section('content')

<h1>Ticket #<?php echo $ticket->id; ?><?php if ($ticket->status == 0) { echo ' - Closed'; } ?></h1>
<h3><?php echo $ticket->topic; ?></h3>
<?php foreach ($ticket->replies as $reply): ?>
	<?php echo $reply->user->username; ?>
	<p><?php echo $reply->reply; ?></p>
<?php endforeach; ?>
@include('layouts.errors')
<?php echo Form::Model($ticket, ['route' => ['reply.update', $ticket->id], 'method' => 'put']); ?>
	<textarea name="reply" maxlength="3000" placeholder="What say you?"></textarea>
	<input type="submit" value="+ Add Reply" class="button success">
<?php echo Form::close(); ?>
<?php if ($ticket->status == 1): ?>
	<?php echo Form::Model($ticket, ['route' => ['support.update', $ticket->id], 'method' => 'put']); ?>
		<input type="submit" value="Close Ticket }" class="button small alert">
	<?php echo Form::close(); ?>
<?php endif; ?>

@stop
