<?php

class AccountTableSeeder extends Seeder {

	public function run() {

		$ran = fopen('/dev/urandom', 'r');
		$string = fread($ran, 32);
		fclose($ran);
		$salt = base64_encode($string);

		$e = 'help';
		$d = '@mailstache.io';

		$account = new Account;
		$account->email = 'help@mailstache.io';
		$account->password = 'password';
//		$account->password = crypt('password', '$6$'.$salt);
		$account->name = 'Mailstache Support';
		$account->user_id = 1;
		$account->domain_id = 1;
		$account->save();

		exec('python /srv/www/app/controllers/python/cmuser.py ' . $e . ' ' . $d);
	}
}
