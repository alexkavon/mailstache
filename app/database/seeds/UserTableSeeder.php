<?php

class UserTableSeeder extends Seeder {

	public function run() {
		$user = new User;

		$user->username = 'mailstache';
		$user->phone = 0;
		$user->password = Hash::make('password');
		$user->level = 1;
		$user->code = mt_rand(10000000, 99999999);
		$user->save();
	}
}
