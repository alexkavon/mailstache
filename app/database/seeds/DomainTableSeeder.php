<?php

class DomainTableSeeder extends Seeder {

	public function run() {
		$domain = new Domain;

		$domain->domain = 'mailstache.io';
		$domain->user_id = 1;
		$domain->verified = 1;
		$domain->save();
	}
}
