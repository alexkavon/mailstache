<?php

use Illuminate\Database\Migrations\Migration;

class CreateDomainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('domains', function($table) {

            $table->increments('id');
            $table->string('domain')->unique();
            $table->text('private_key')->nullable();
            $table->text('public_key')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('gid')->default(8);
            $table->integer('verified')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('domains');
	}

}
