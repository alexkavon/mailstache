<?php

use Illuminate\Database\Migrations\Migration;

class CreateAliasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('aliases', function($table) {

			$table->increments('id');
			$table->string('alias');
			$table->string('email');
			$table->integer('account_id')->unsigned();

			$table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('aliases');
	}

}
