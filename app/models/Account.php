<?php

class Account extends Eloquent {

	protected $table = 'accounts';

	protected $hidden = ['password', 'domain_id', 'user_id'];

	protected $fillable = ['email', 'password', 'domain_id', 'user_id'];

	public function domains() {

		return $this->belongsTo('Domain');
	}

	public function users() {

		return $this->belongsTo('User');
	}

	public function aliases() {

		return $this->hasMany('Alias');
	}
}
