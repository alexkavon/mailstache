<?php

class Reply extends Eloquent {

	protected $table = 'replies';

	protected $hidden = ['reply', 'ticket_id','user_id'];
	protected $fillable = ['user_id', 'ticket_id', 'reply'];

	public function tickets() {

		return $this->belongsTo('Ticket');
	}

	public function user() {

		return $this->belongsTo('User');
	}
}
