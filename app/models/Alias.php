<?php

class Alias extends Eloquent {

	protected $table = 'aliases';

	protected $hidden = ['account_id'];

	protected $fillable = ['alias', 'email', 'account_id'];

	public $timestamps = false;

	public function accounts() {

		return $this->belongsTo('Account');
	}
}
