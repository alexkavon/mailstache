<?php

class Domain extends Eloquent {

	protected $table = 'domains';

	protected $hidden = ['private_key', 'public_key', 'user_id'];
	protected $fillable = ['user_id','domain', 'private_key', 'verified'];

	public function accounts() {

		return $this->hasMany('Account');
	}

	public function users() {

		return $this->belongsTo('User');
	}
}
