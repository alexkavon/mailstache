<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Laravel\Cashier\BillableTrait;
use Laravel\Cashier\BillableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface, BillableInterface {

	use BillableTrait;

	protected $table = 'users';
	protected $hidden = ['password'];
	protected $dates = ['trial_ends_at', 'subscription_ends_at'];
	protected $fillable = ['username', 'phone', 'password', 'group', 'verified'];

	public function getDates() {

		return ['trial_ends_at', 'subscription_ends_at', 'created_at', 'updated_at'];
	}

	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getRememberToken()
	{
		return $this->remember_token;
	}

	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	public function getReminderEmail()
	{
		return $this->email;
	}

	public function domains() {

		return $this->hasMany('Domain');
	}

	public function accounts() {

		return $this->hasMany('Account');
	}

	public function replies() {

		return $this->hasMany('Reply');
	}
}
