<?php

class Ticket extends Eloquent {

	protected $table = 'tickets';

	protected $hidden = ['topic', 'user_id'];
	protected $fillable = ['user_id', 'topic', 'status'];

	public function replies() {

		return $this->hasMany('Reply');
	}

	public function users() {

		return $this->belongsTo('User');
	}
}
