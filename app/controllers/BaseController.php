<?php

class BaseController extends Controller {

	public function __construct() {

		$this->beforeFilter('csrf', ['on' => ['post','put','delete']]);
	}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
}
