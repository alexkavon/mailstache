<?php

class BillingController extends BaseController {

	public function __construct() {

		$this->beforeFilter('upaccount', ['only' => ['upgrade']]);
		$this->beforeFilter('updomain', ['only' => ['upgrade']]);
	}

	public function index() {

		$invoices = Auth::user()->invoices();

		return View::make('billing')->with('invoices', $invoices);
	}

	public function create() {

		$user = Auth::user();
		$plan = Input::get('plan');
		$token = Input::get('stripeToken');

		try {

			$user->subscription($plan)->create($token);
		} catch(Stripe_CardError $e) {

			$body = $e->getJsonBody();
			$err = $body['error'];

			return Redirect::back()->with('message', $err['message']);
		} catch (Stripe_InvalidRequestError $e) {

			$body = $e->getJsonBody();
			$err = $body['error'];

			return Redirect::back()->with('message', $err['message']);
		} catch(Stripe_Error $e) {

			$body = $e->getJsonBody();
			$err = $body['error'];

			return Redirect::back()->with('message', $err['message']);
		}

		return Redirect::route('domains.index');
	}

	public function upgrade() {

		$plan = Input::get('plan');
		$accounts = Account::where('user_id', Auth::user()->id)->get();

		Auth::user()->subscription($plan)->swap();

		foreach ($accounts as $account) {
			Queue::push('AccountCreate@upgrade', ['e' => $account->email, 'p' => $plan]);
		}

		return Redirect::back()->with('message', 'Your mailstache account has been upgraded to the ' . $plan . ' plan.');
	}

	public function download($invoice) {

		$user = Auth::user();

		return $user->downloadInvoice($invoice, [
			'vendor'  => 'Infines',
			'product' => 'Mailstache',
		]);
	}

	public function cancel() {

		Auth::user()->subscription()->cancel();

		return Redirect::back();
	}
}
