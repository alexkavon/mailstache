<?php

class AccountController extends BaseController {

	public function __construct() {

		$this->beforeFilter('account', ['only' => ['store']]);
	}

	public function index() {

		$user = User::with('domains')->where('id', Auth::user()->id)->first();

		if ($user->domains->count() > 0) {

			return View::make('accounts.new')->with('domains', Domain::where('user_id', Auth::user()->id)->lists('domain', 'domain'));
		} else {

			return Redirect::route('domains.index');
		}
	}

	public function store() {

		$i = ['name' => Input::get('name'),
			'username' => strtolower(Input::get('username')),
			'email' => strtolower(Input::get('username')) . '@' . Input::get('domain'),
			'password' => Input::get('password'),
			'password_confirmation' => Input::get('password_confirmation')];

		$r = ['name' => 'required|max:70|regex:/^([a-z0-9-_\s]+)$/i',
			'username' => 'required|alpha_dash',
			'email' => 'required|unique:accounts,email|email',
			'password' => 'required|min:8|confirmed',
			'password_confirmation' => 'same:password'];

		$v = Validator::make($i, $r);

		$domain = Domain::with('accounts')->where('domain', Input::get('domain'))->where('verified', 1)->first();

		if (! $domain) {

		//Make sure they are verified!
			return Redirect::back()->with('message', 'You need to verify your domain before you can add an account to it!');
		} elseif ($v->fails()) {

		//Check to see if everything works!
			return Redirect::back()->withErrors($v);
		} else {

			$account = new Account();
			$account->name = Input::get('name');
			$account->email = strtolower(Input::get('username')) . '@' . Input::get('domain');
			$account->password = Hash::make(Input::get('password'));
			$account->domain_id = $domain->id;
			$account->user_id = Auth::user()->id;
			$account->save();

			$u = strtolower(Input::get('username'));
			$d = '@' . Input::get('domain');
			$e = $u . $d;
			$p = Auth::user()->stripe_plan;

			Queue::bulk(['AccountCreate@fire', 'AccountCreate@folder', 'AccountCreate@perm', 'AccountCreate@sieve', 'AccountCreate@upgrade'], ['u' => $u, 'd' => $d, 'p' => $p, 'e' => $e]);

			return Redirect::route('domains.show', [$account->domain_id]);
		}
	}

	public function update($id) {

		$account = Account::where('id', $id)->where('user_id', Auth::user()->id)->first();
		$i = Input::only('password', 'password_confirmation');
		$r = ['password' => 'required|min:8|confirmed',
			'password_confirmation' => 'same:password'];
		$v = Validator::make($i,$r);

		if ($v->fails()) {

			return Redirect::back()->withErrors($v);
		} else {

			$account->password = Hash::make(Input::get('password'));
			$account->touch();
			$account->save();

			return Redirect::back()->with('message', 'Email account password updated.');
		}
	}

	public function show($id) {

		$account = Account::where('id', $id)->where('user_id', Auth::user()->id)->first();

		return View::make('accounts.show')->with('account', $account)->with('aliases', $account->aliases)->with('domains', Domain::where('user_id', Auth::user()->id)->lists('domain', 'domain'));;
	}

	public function destroy($id) {

		$account = Account::where('id',$id)->where('user_id',Auth::user()->id)->first();
		$i = ['delete' => strtolower(Input::get('delete'))];
		$r = ['delete' => 'max:6|in:delete|required|alpha'];
		$v = Validator::make($i,$r);

		if ($v->fails()) {

			return Redirect::back()->with('message', 'Sorry! You must confirm the deleting of this email account.');
		}

		Queue::push('AccountCreate@destroy', ['e' => $account->email]);
		$account->delete();

		return Redirect::route('domains.index');
	}
}
