<?php

class AuthController extends BaseController {

	public function login() {

		if (Auth::check()) {

			return Redirect::to('domains');
		} else {

			return View::make('login');
		}
	}

	public function auth() {

		$input = ['username' => Input::get('username'),
							'password' => Input::get('password')];

		$rules = ['username' => 'required|alpha_dash',
			'password' => 'required'];

		$v = Validator::make($input, $rules);

		if ($v->fails()) {

			return Redirect::back()->withErrors($v);
		} elseif (Auth::attempt($input)) {

			return Redirect::to('domains');
		} else {

			return Redirect::back()->with('message', 'Sorry! Those credentials don\'t corroborate.');
		}
	}

	public function logout() {

		Auth::logout();
		return Redirect::to('login');
	}
}
