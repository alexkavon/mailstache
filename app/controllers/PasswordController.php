<?php

class PasswordController extends BaseController {

	public function reset() {

		if (Auth::check()) {

			return Redirect::to('domains');
		} else {

			return View::make('user.reset');
		}
  }

	public function passreset() {

		$input = Input::only('username');
		$rules = ['username' => 'exists:users,username|max:15'];
		$v = Validator::make($input,$rules);

		if ($v->fails()) {

			return Redirect::back()->withErrors($v);
		} else {

			$user = User::where('username', Input::get('username'))->first();

			try {

				$sid = 'ACd79c3e68930826b0829bd135f3d6a865';
				$token = '30449b045f5cade4b89ee63f41db5cc4';

				$client = new Services_Twilio($sid, $token);
				$client->account->messages->sendMessage(
					'+14066620011',
					'+' . $user->phone,
					'Your Mailstache account has requested a password reset! If you did not request this reset then ignore this message. Your password reset code: ' . $user->code
				);
			} catch(Services_Twilio_RestException $e) {

				return Redirect::back()->with('message', $e->getMessage());
			}

			return View::make('user.passreset')->with('username', Input::get('username'));
		}
	}

	public function sendreset() {

		$username = Input::get('username');
		$user = User::where('username', $username)->first();
		$input = Input::only('code');
		$rules = ['code' => 'required'];
		$v = Validator::make($input,$rules);

		if ($v->fails()) {

			return Redirect::back()->withErrors($v);
		} elseif (Input::get('code') == $user->code) {

			$newpass = substr(md5($username . time()), 5, 12);
			$user->password = Hash::make($newpass);
			$user->touch();
			$user->save();

			try {
				$sid = 'ACd79c3e68930826b0829bd135f3d6a865';
				$token = '30449b045f5cade4b89ee63f41db5cc4';

				$client = new Services_Twilio($sid, $token);
				$client->account->messages->sendMessage(
					'+14066620011',
					'+' . $user->phone,
					'Your Mailstache account password has been reset! If you did not authorize this, please contact our support team immediately. Your new password is: ' . $newpass
				);
			} catch(Services_Twilio_RestException $e) {

				return Redirect::back()->with('message', $e->getMessage());
			}

			return Redirect::to('login')->with('message', 'Successful password reset!');

		} else {

			return Redirect::back()->with('message', 'Sorry wrong code!');
		}
	}
}
