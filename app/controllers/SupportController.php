<?php

class SupportController extends BaseController {

	public function index() {

		return View::make('tickets.index')->with('tickets', Ticket::where('user_id', Auth::user()->id)->paginate(10));
	}

	public function store() {

		$input = ['topic' => Input::get('topic'),
			'reply' => Input::get('reply')];

		$rules = ['topic' => 'required|max:50',
			'reply' => 'required|max:3000'];

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {

			return Redirect::back()->withErrors($validation);
		} else {

			$ticket = new Ticket();
			$ticket->topic = htmlentities(Input::get('topic'),ENT_QUOTES,'UTF-8');
			$ticket->user_id = Auth::user()->id;
			$ticket->save();

			$reply = new Reply();
			$reply->reply = htmlentities(Input::get('reply'),ENT_QUOTES,'UTF-8');
			$reply->ticket_id = $ticket->id;
			$reply->user_id = Auth::user()->id;
			$reply->save();

			return Redirect::back();
		}
	}

	public function show($id) {

		$ticket = Ticket::where('id', $id)->where('user_id',Auth::user()->id)->first();

		return View::make('tickets.show')->with('ticket', $ticket)->with('replies', $ticket->replies()->orderBy('created_at', 'DESC')->get());
	}

	public function update($id) {

		$ticket = Ticket::where('id',$id)->where('user_id',Auth::user()->id)->first();

		$ticket->status = 0;
		$ticket->touch();
		$ticket->save();

		return Redirect::to('support')->with('message', 'Ticket ' . $id . ' has been closed.');
	}
}
