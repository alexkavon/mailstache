<?php

class AliasController extends BaseController {

	public function store() {

		$input = ['email' => strtolower(Input::get('alias')) . '@' . Input::get('domain'),
			'alias' => strtolower(Input::get('alias'))];

		$rules = ['email' => 'required|email|unique:aliases,alias|unique:accounts,email',
			'alias' => 'required|alpha_dash|max:70'];

		$validation = Validator::make($input, $rules);

		$account = Account::with('aliases')->where('email', Input::get('email'))->first();

		if ($account->aliases->count() == 5) {

			return Redirect::back()->with('message', 'Sorry! There is a limit of 5 aliases per account!');
		} elseif ($validation->fails()) {

			return Redirect::back()->withErrors($validation);
		} else {

			$alias = new Alias();
			$alias->alias = strtolower(Input::get('alias')) . '@' . Input::get('domain');
			$alias->email = Input::get('email');
			$alias->user_id = Auth::user()->id;
			$alias->account_id = Account::where('email', Input::get('email'))->first()->id;
			$alias->save();
		}

		return Redirect::back();
	}

	public function destroy($id) {

		$alias = Alias::where('id',$id)->where('user_id',Auth::user()->id)->first();

		$input = ['delete' => strtolower(Input::get('delete'))];
		$rules = ['delete' => 'max:6|in:delete|required|alpha'];
		$v = Validator::make($input,$rules);

		if ($v->fails()) {

			return Redirect::back()->with('message', 'Sorry! You must confirm the removal of an alias.');
		} else {

			$alias->delete();
			return Redirect::back()->with('message', 'Alias successfully deleted!');
		}
	}
}
