<?php

class UserController extends BaseController {

	public function index() {

		return View::make('user.settings')->with('user', Auth::user());
	}

	public function password() {

		$user = Auth::user();

		$input = Input::only('password','password_confirmation');

		$rules = ['password' => 'required|min:8|confirmed',
			'password_confirmation' => 'same:password'];

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {

			return Redirect::to('settings')->withErrors($validation);
		} else {

			$user->password = Hash::make(Input::get('password'));
			$user->touch();
			$user->save();

			return Redirect::to('settings')->with('message', 'Your password has been updated.');
		}
	}

	public function phone() {

		$user = Auth::user();

		$input = Input::only('phone');
		$rules = ['phone' => 'required|unique:users,phone|digitsbetween:8,15|numeric'];

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {

			return Redirect::to('settings')->withErrors($validation);
		} else {

			$user->phone = Input::get('phone');
			$user->level = 0;
			$user->touch();
			$user->save();
		}

		return Redirect::to('settings')->with('message', 'Your phone number has been updated.');
	}

	public function verify() {

		$user = Auth::user();

		$input = Input::only('code');
		$rules = ['code' => 'required'];
		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {

			return Redirect::back()->withErrors($validation);
		} elseif (Input::get('code') == Auth::user()->code) {

			$user->level = 1;
			$user->code = mt_rand(10000000, 99999999);
			$user->touch();
			$user->save();
		} else {

			return Redirect::back()->withErrors('This code doesn\'t corroborate!');
		}

		return Redirect::to('domains');
	}

	public function message() {

		$user = Auth::user();
		$user->code = mt_rand(10000000, 99999999);
		$user->touch();
		$user->save();

		try {

			$sid = 'ACd79c3e68930826b0829bd135f3d6a865';
			$token = '30449b045f5cade4b89ee63f41db5cc4';

			$client = new Services_Twilio($sid, $token);
			$client->account->messages->sendMessage(
				'+14066620011',
				'+' . $user->phone,
				'Your Mailstache code is: ' . $user->code
			);
		} catch(Services_Twilio_RestException $e) {

			return Redirect::back()->with('message', $e->getMessage());
		}

		return Redirect::to('domains');
	}

	public function destroy() {

		$user = Auth::user();
		$input = ['delete' => strtolower(Input::get('delete'))];
		$rules = ['delete' => 'max:6|in:delete|required|alpha'];
		$v = Validator::make($input,$rules);

		if ($v->fails()) {

			return Redirect::back()->with('message', 'Sorry! You must confirm the closing of an account.');
		}

		foreach ($user->accounts as $account) {

			Queue::push('AccountCreate@destroy', ['e' => $account->email]);
		}

		if ($user->subscribed()){

			$user->subscription()->cancel();
		}

		$user->delete();

		return Redirect::to('/');
	}
}
