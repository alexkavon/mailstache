<?php

class ReplyController extends BaseController {

	public function update($id) {

		$input = Input::only('reply');

		$rules = ['reply' => 'required|max:3000'];

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {

			return Redirect::back()->withErrors($validation);
		} else {

			$reply = new Reply();
			$ticket = Ticket::where('id',$id)->where('user_id',Auth::user()->id)->first();

			$reply->reply = htmlentities(Input::get('reply'),ENT_QUOTES,'UTF-8');
			$reply->ticket_id = $ticket->id;
			$reply->user_id = Auth::user()->id;
			$reply->save();

			$ticket->status = 1;
			$ticket->touch();
			$ticket->save();
		}

    return Redirect::back();
	}
}
