import sys, time
from imaplib import *

user = sys.argv[1]
domain = sys.argv[2]

while True:
	server = IMAP4('localhost', 143)
	resp, data = server.login('cyrus-frontend', 'password')
	try:
		if resp == 'OK':
			server.create('user/' + user + '/Sent' + domain)
			server.create('user/' + user + '/Drafts' + domain)
			server.create('user/' + user + '/Spam' + domain)
			server.create('user/' + user + '/Trash' + domain)
			time.sleep(6)
	except server.error(), e:
		continue
	except server.abort(), e:
		continue
	server.logout()
	break
