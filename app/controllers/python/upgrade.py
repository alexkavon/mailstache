import sys, time
from imaplib import *

email = sys.argv[1]
plan = sys.argv[2]

while True:
	server = IMAP4('localhost', 143)
	resp, data = server.login('cyrus-frontend', 'password')
	try:
		if resp == 'OK':
			resp = server.list('""', 'user/' + email)
			if resp == ('OK', ['(\\HasChildren) "/" user/' + email]):
				if plan == 'free':
					server.setquota('user/' + email, '(STORAGE 1048576)')
				elif plan == 'toothbrush':
					server.setquota('user/' + email, '(STORAGE 5242880)')
				elif plan == 'chevron':
					server.setquota('user/' + email, '(STORAGE 10485760)')
				elif plan == 'fumanchu':
					server.setquota('user/' + email, '(STORAGE 10485760)')
				time.sleep(6)
				server.logout()
	except server.error(), e:
		continue
	except server.abort(), e:
		continue
	break
