from sievelib.factory import FiltersSet
from sievelib.sievelib import MANAGESIEVE
import sys, time

user = sys.argv[1]
domain = sys.argv[2]

fs = FiltersSet("sieve")

fs.addfilter("xspam",
	[("X-Spam-Flag",":contains","YES"),],
	[("fileinto","Spam"),])

fs = fs.__str__()

c = MANAGESIEVE("localhost",4190)
c.login("PLAIN",user + domain,"password",admin="cyrus-frontend")
c.putscript("sieve",fs)
c.setactive(scriptname="sieve")
c._close()
time.sleep(6)
