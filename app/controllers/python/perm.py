import sys, time
from imaplib import *

user = sys.argv[1]
domain = sys.argv[2]

while True:
	server = IMAP4('localhost', 143)
	resp, data = server.login('cyrus-frontend', 'password')
	try:
		if resp == 'OK':
			resp = server.list('""', 'user/' + user + domain)
			if resp == ('OK', ['(\\HasChildren) "/" user/' + user + domain]):
				server.setacl('user/' + user + domain, 'cyrus-frontend', 'c')
				server.setacl('user/' + user + domain, user + domain,'lrswipkxte')
				server.setacl('user/' + user + '/Drafts' + domain, user + domain, 'lrswipte')
				server.setacl('user/' + user + '/Sent' + domain, user + domain, 'lrswipte')
				server.setacl('user/' + user + '/Spam' + domain, user + domain, 'lrswipte')
				server.setacl('user/' + user + '/Trash' + domain, user + domain, 'lrswipte')
				server.setannotation('user/' + user + '/Trash' + domain, '/vendor/cmu/cyrus-imapd/expire', '("value.shared" "30")')
				server.setannotation('user/' + user + '/Spam' + domain, '/vendor/cmu/cyrus-imapd/expire', '("value.shared" "30")')
				time.sleep(6)
				server.logout()
	except server.error(), e:
		continue
	except server.abort(), e:
		continue
	break
