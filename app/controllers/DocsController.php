<?php

class DocsController extends BaseController {

	public function start() {

		return View::make('docs.start');
	}

	public function domain() {

		return View::make('docs.domain');
	}

	public function mailbox() {

		return View::make('docs.mailbox');
	}

	public function client() {

		return View::make('docs.client');
	}

	public function spam() {

		return View::make('docs.spam');
	}

	public function apiAccount() {

		return View::make('docs.apiAccount');
	}
}
