<?php

class DomainController extends BaseController {

	public function __construct() {

		$this->beforeFilter('domain', ['only' => ['store']]);
	}

	public function index() {

		return View::make('domains.index')->with('domains', Domain::where('user_id', Auth::user()->id)->orderBy('domain', 'ASC')->paginate(10));
	}

	public function create() {

		return View::make('domains.new');
	}

	public function store() {

		$domain_name = strtolower(Input::get('domain'));
		$domain_name = str_replace(['http://', '/'], '', $domain_name);

		$input = ['domain' => $domain_name,
							'url' => 'http://' . $domain_name];

		$rules = ['domain' => 'required|unique:domains,domain|max:253|regex:/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i',
			'url' => 'url'];

		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {

			$newkey = openssl_pkey_new(['private_key_bits' => 1024]);
			openssl_pkey_export($newkey, $privkey);
			$pubkey = openssl_pkey_get_details($newkey);
			$pubkey = $pubkey['key'];
			$privkey = str_replace(['-----BEGIN PRIVATE KEY-----' . PHP_EOL, PHP_EOL .  '-----END PRIVATE KEY-----'], ['-----BEGIN RSA PRIVATE KEY-----' . PHP_EOL, PHP_EOL . '-----END RSA PRIVATE KEY-----'], $privkey);
			$pubkey = str_replace(['-----BEGIN PUBLIC KEY-----' . PHP_EOL, PHP_EOL . '-----END PUBLIC KEY-----'], ['"v=DKIM1 k=rsa; p=', '"'], $pubkey);

			$domain = new Domain();
			$domain->domain = $domain_name;
			$domain->public_key = $pubkey;
			$domain->private_key = $privkey;
			$domain->user_id = Auth::user()->id;
			$domain->save();

			return Redirect::route('domains.index');
		} else {

			return Redirect::route('domains.create')->withErrors($validation);
		}
	}

	public function show($id) {

		$domain = Domain::where('id', $id)->where('user_id', Auth::user()->id)->first();

		if ($domain->verified == 0) {

			return View::make('domains.verify')->with('domain', $domain);
		} else {

			return View::make('domains.show')->with('domain', $domain)->with('accounts', $domain->accounts);
		}
	}

	public function destroy($id) {

		$domain = Domain::where('id', $id)->where('user_id', Auth::user()->id)->first();
		$input = ['delete' => strtolower(Input::get('delete'))];
		$rules = ['delete' => 'max:6|in:delete|required|alpha'];
		$v = Validator::make($input,$rules);

		if ($v->fails()) {

			return Redirect::back()->with('message', 'Sorry! You must confirm the deleting of this domain.');
		}

		foreach ($domain->accounts as $account) {

			Queue::push('AccountCreate@destroy', ['e' => $account->email]);
		}

		$domain->delete();

		return Redirect::route('domains.index')->with('message', 'The domain ' . $domain->domain . ' and associated data (mailboxes and aliases) have been deleted!');
	}

	public function update($id) {

		$domain = Domain::where('id',$id)->where('user_id',Auth::user()->id)->first();
		$hosts = [];
		$mxrecords = ['mx.mailstache.io', 'mx2.mailstache.io', 'mx3.mailstache.io', 'mx4.mailstache.io', 'mx5.mailstache.io'];
		getmxrr($domain->domain, $hosts);
		$hosts = array_map('strtolower', $hosts);

		foreach ($mxrecords as $item) {

			if (! in_array($item, $hosts)) {
				return Redirect::back()->with('message', 'Missing record: ' . $item);
			}
		}

		$domain->verified = 1;
		$domain->save();

		return Redirect::back();
	}
}
