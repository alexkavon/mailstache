<?php

return array(
    /**
     * SID - Your Twilio Account SID #
     */
    "sid" => "ACd79c3e68930826b0829bd135f3d6a865",

    /**
     * token - Access token that can be found in your Twilio dashboard
     */
    "token" => "30449b045f5cade4b89ee63f41db5cc4",

    /**
     * from - The Phone number registered with Twilio that your SMS & Calls will come from
     */
    "from" => "+14066620011"
);
